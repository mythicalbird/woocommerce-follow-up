<?php

class Wcfu_Waboxapp
{
	
	private $url = 'https://www.waboxapp.com/api';
	private $token;
	private $uid;
	public $msg;

	public function __construct() {
	    
		$this->token = get_option('wcfu_waboxapp_token', '');
		$this->uid = get_option('wcfu_waboxapp_uid', '');
		$this->msg = get_option('wcfu_waboxapp_msg', '');
		
	}
	
	public function send_chat($nohp, $unique_id, $teks = '') {
		
		$url = $this->url . '/send/chat';
		
		if( $teks == '' ) {
			$teks = $this->msg;
		}
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "token=".$this->token."&uid=".$this->uid."&to=".$nohp."&custom_uid=".trim($unique_id)."&text=".urlencode($teks));
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT,30);
		curl_setopt($ch, CURLOPT_POST, 1);
		$results = curl_exec($ch);
		curl_close($ch);

		return json_decode($results, true);
		
	}
	
	public function get_order_message_tags( $data = array() ) {
		
		$tags = array(
			'customer_name'		=> $data['customer_name'],
			'customer_phone'	=> $data['customer_phone'],
			'customer_email'	=> $data['customer_email'],
			'invoice_number'	=> $data['invoice_number'],
			'order_total'		=> $data['order_total'],
			'order_status'		=> $data['order_status'],
			'order_date'		=> $data['order_date'],
			'payment_method'	=> $data['payment_method'],
			'order_items'		=> '',
		);

		$items = $data['order_items'];
		$item_str = '';
		foreach ($items as $item) {
			$p = $item->get_data();
			$item_str .= $p['name'] . ' (Qty: ' . $p['quantity'] . '), ';
		}

		$tags['order_items'] = rtrim($item_str, ', ');
		
		return $tags;
		
	}

}