<?php

class Wcfu_Sms_Notifikasi
{
	
	private $account_type;
	private $userkey;
	private $passkey;

	public function __construct() {
		
	    $this->account_type = get_option('wcfu_sms_notifikasi_account_type');
			$this->userkey = get_option('wcfu_sms_notifikasi_userkey');
			$this->passkey = get_option('wcfu_sms_notifikasi_passkey');
		
	}

	public function send_sms($nohp, $pesan = '') {
		
		$url = "http://".$this->account_type.".sms-notifikasi.com/apps/smsapi.php";

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "userkey=".$this->userkey."&passkey=".$this->passkey."&nohp=".$nohp.
		"&pesan=".urlencode($pesan));
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT,30);
		curl_setopt($ch, CURLOPT_POST, 1);
		$results = curl_exec($ch);
		curl_close($ch);

		return $results;
		
	}	

}