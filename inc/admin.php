<?php

add_action('admin_menu', 'wcfu_create_setting_menu' );
function wcfu_create_setting_menu() {
    add_submenu_page( 
        'woocommerce', 
        __('Woocommerce: Notifikasi & Follow Up', 'woocommerce' ),
        __('Notifikasi/Follow Up', 'woocommerce' ),
        'manage_options', 
        'wcfu-notifikasi-follow-up',
        'wcfu_settings_page'
    );
    add_action( 'admin_init', 'wcfu_register_settings' );
}


function wcfu_register_settings() {

    register_setting( 'wcfu-settings-group', 'wcfu_notifikasi_enabled' );
		register_setting( 'wcfu-settings-group', 'wcfu_notifikasi_jam' );
		register_setting( 'wcfu-settings-group', 'wcfu_notifikasi_menit' );
		register_setting( 'wcfu-settings-group', 'wcfu_notifikasi_delay' );
		register_setting( 'wcfu-settings-group', 'wcfu_notifikasi_order_status' );
		register_setting( 'wcfu-settings-group', 'wcfu_notifikasi_pesan' );
		register_setting( 'wcfu-settings-group', 'wcfu_sms_notifikasi_enabled' );
    register_setting( 'wcfu-settings-group', 'wcfu_sms_notifikasi_account_type' );
		register_setting( 'wcfu-settings-group', 'wcfu_sms_notifikasi_userkey' );
		register_setting( 'wcfu-settings-group', 'wcfu_sms_notifikasi_passkey' );
		register_setting( 'wcfu-settings-group', 'wcfu_waboxapp_enabled' );
		register_setting( 'wcfu-settings-group', 'wcfu_waboxapp_token' );
		register_setting( 'wcfu-settings-group', 'wcfu_waboxapp_uid' );
		register_setting( 'wcfu-settings-group', 'wcfu_waboxapp_msg' );

}

function wcfu_settings_page() {
    global $wpdb; ?>
    <div class="wrap">
    <div id="icon-themes" class="icon32"></div>
    <h2><?php _e('Follow-up Settings', 'woocommerce'); ?></h2>

			<?php settings_errors(); ?>
			<?php $active_tab = ( !empty( $_GET[ 'tab' ] ) ) ? $_GET[ 'tab' ] : 'options'; ?>

			<h2 class="nav-tab-wrapper">
					<a href="#options" data-toggle="options" class="nav-tab <?php echo $active_tab == 'options' ? 'nav-tab-active' : ''; ?>">Pengaturan Follow Up</a>
					<a href="#api" data-toggle="api" class="nav-tab <?php echo $active_tab == 'api' ? 'nav-tab-active' : ''; ?>">Pengaturan Notifikasi & API</a>
			</h2>

			<form class="wcfu-options-form" method="post" action="options.php">
					<?php settings_fields( 'wcfu-settings-group' ); ?>
					<?php do_settings_sections( 'wcfu-settings-group' ); ?>

					<div id="tab-options" class="wcfu-tab-content">
						<?php include __DIR__ . '/../views/wcfu-tab-options.php'; ?>
					</div>
					<div id="tab-api" class="wcfu-tab-content" style="display:none">
						<?php include __DIR__ . '/../views/wcfu-tab-api.php'; ?>
					</div>
			</form>
    
			<script>
				jQuery(document).ready(function(){
					
					jQuery('.nav-tab-wrapper .nav-tab, .btn-100').on( 'click', function(){
							jQuery('.nav-tab').removeClass('nav-tab-active');
							jQuery('.wcfu-tab-content').hide(0);
							jQuery(this).addClass('nav-tab-active');
							var tab = jQuery(this).data('toggle');
							jQuery('#tab-'+tab).show('fast');
					});
					
					jQuery(".wcfu-options-form").submit(function(){ 
							var url = window.location.href;

							jQuery.post(
									ajaxurl, 
									{
											'action': 'wcfu_ajax_update_cron',
											'notifikasi': jQuery("#wcfu_notifikasi_enabled").val()
									},
									function(response){
										location.reload();
									});
					});
					
				});
			</script>
			
    </div>
<?php 
} 

function wcfu_ajax_update_cron() {
		wp_clear_scheduled_hook('wcfu_follow_up_hook');
		$enabled = intval($_POST['notifikasi']);
		if( $enabled == 1 ) {
			wp_schedule_event( time(), 'five_seconds', 'wcfu_follow_up_hook' );
		}
    wp_die();
}
add_action( 'wp_ajax_wcfu_ajax_update_cron', 'wcfu_ajax_update_cron' );