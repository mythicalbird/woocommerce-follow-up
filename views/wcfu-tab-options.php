<table class="form-table">
		<tr>
				<th scope="row"></th>
				<td>
						<label>
							<input type="checkbox" id="wcfu_notifikasi_enabled" name="wcfu_notifikasi_enabled" value="1" <?php checked( get_option( 'wcfu_notifikasi_enabled' ), 1 ); ?> />
							Aktifkan notifikasi follow up.
						</label>
				</td>
		</tr>
		<tr>
				<th scope="row">Waktu/Jam</th>
				<td>
						<select id="wcfu_notifikasi_jam" name="wcfu_notifikasi_jam">
							<?php 
							for ( $i = 0; $i <= 23; $i++ ){
								echo '<option value="' . $i . '" ' . selected( get_option('wcfu_notifikasi_jam', ''), $i ) .'>' . sprintf("%02d", $i) . '</option>';
							}  
							?>
						</select>
						:
						<select id="wcfu_notifikasi_menit" name="wcfu_notifikasi_menit">
							<?php 
							for ( $i = 0; $i <= 59; $i++ ){
								echo '<option value="' . $i . '" ' . selected( get_option('wcfu_notifikasi_menit', ''), $i ) .'>' . sprintf("%02d", $i) . '</option>';
							}  
							?>
						</select>
						<p>
							Tiap jam berapa notifikasi akan dikirim (format waktu 24 jam). <br>Format = jam:menit, contoh: <code>12:00</code> (Jam 12.00)
						</p>
				</td>
		</tr>
		<tr>
				<th scope="row">Delay</th>
				<td>
						<input type="number" id="wcfu_notifikasi_delay" name="wcfu_notifikasi_delay" value="<?php echo esc_attr( get_option('wcfu_notifikasi_delay', '') ); ?>" />
						<p>Interval notifikasi dikirim per order (dalam <strong>detik/second</strong>).<br>Contoh: <code>60</code>. Delay 60 detik atau 1 menit.</p>
				</td>
		</tr>
		<tr>
				<th scope="row">Order Status</th>
				<td>
						<select id="wcfu_notifikasi_order_status" name="wcfu_notifikasi_order_status">
							<?php 
							$order_statuses = wc_get_order_statuses();
							foreach ( $order_statuses as $k => $v ){
								echo '<option value="' . $k . '" ' . selected( get_option('wcfu_notifikasi_order_status', ''), $k ) .'>' . __($v, 'woocommerce') . '</option>';
							}  
							?>
						</select>
				</td>
		</tr>
		<tr>
				<th scope="row">Isi Pesan Follow Up</th>
				<td>
						<textarea id="wcfu_notifikasi_pesan" cols="40" rows="6" name="wcfu_notifikasi_pesan"><?php echo esc_attr( get_option('wcfu_notifikasi_pesan', '') ); ?></textarea>
						<p>
						Tags: 
							<code>%customer_name%</code>, 
							<code>%customer_phone%</code>,
							<code>%customer_email%</code>,
							<code>%invoice_number%</code>,
							<code>%order_total%</code>, 
							<code>%order_status%</code>,
							<code>%order_date%</code>,
							<code>%payment_method%</code>
							<code>%order_items%</code>
						</p>
				</td>
		</tr>
</table>
<?php submit_button(); ?>