<h3>Whaboxapp (Whatsapp)</h3>
<table class="form-table">
		<tr>
				<th scope="row"></th>
				<td>
						<label>
							<input type="checkbox" id="wcfu_waboxapp_enabled" name="wcfu_waboxapp_enabled" value="1" <?php checked( get_option( 'wcfu_waboxapp_enabled' ), 1 ); ?> />
							Aktifkan notifikasi Whatsapp.
						</label>
				</td>
		</tr>
		<tr>
				<th scope="row">Token</th>
				<td>
						<input type="text" id="wcfu_waboxapp_token" name="wcfu_waboxapp_token" value="<?php echo esc_attr( get_option('wcfu_waboxapp_token', '') ); ?>" />
				</td>
		</tr>
		<tr>
				<th scope="row">Nomor HP</th>
				<td>
						<input type="text" id="wcfu_waboxapp_uid" name="wcfu_waboxapp_uid" value="<?php echo esc_attr( get_option('wcfu_waboxapp_uid', '') ); ?>"/>
				</td>
		</tr>
		<tr>
				<th scope="row">Isi Pesan Notifikasi</th>
				<td>
						<textarea id="wcfu_waboxapp_msg" cols="40" rows="6" name="wcfu_waboxapp_msg"><?php echo esc_attr( get_option('wcfu_waboxapp_msg', '') ); ?></textarea>
						<p>
						Tags: 
							<code>%customer_name%</code>, 
							<code>%customer_phone%</code>,
							<code>%customer_email%</code>,
							<code>%invoice_number%</code>,
							<code>%order_total%</code>, 
							<code>%order_status%</code>,
							<code>%order_date%</code>,
							<code>%payment_method%</code>
							<code>%order_items%</code>
						</p>
				</td>
		</tr>
		<tr>
				<th scope="row"></th>
				<td>
						<label>
							<input type="checkbox" id="wcfu_sms_notifikasi_enabled" name="wcfu_sms_notifikasi_enabled" value="1" <?php checked( get_option( 'wcfu_sms_notifikasi_enabled' ), 1 ); ?> />
							Kirim sms jika whatsapp gagal terkirim (via SMS Notifikasi API).
						</label>
				</td>
		</tr>
</table>

<div id="wcfu-notifikasi-sms" style="<?php echo ( get_option('wcfu_sms_notifikasi_enabled') == 1 ) ? 'display:block' : 'display:none'; ?>">
	<hr>
	<h3>SMS Notifikasi</h3>
	<table class="form-table">
			<tr>
					<th scope="row">Tipe Akun</th>
					<td>
							<select id="wcfu_sms_notifikasi_account_type" name="wcfu_sms_notifikasi_account_type">
									<option value="reguler" <?php selected(get_option('wcfu_sms_notifikasi_account_type', ''), 'reguler'); ?>>One Way GSM</option>
									<option value="masking" <?php selected(get_option('wcfu_sms_notifikasi_account_type', ''), 'masking'); ?>>One Way Masking</option>
							</select>
					</td>
			</tr>
			<tr>
					<th scope="row">User Key</th>
					<td>
							<input type="text" id="wcfu_sms_notifikasi_userkey" name="wcfu_sms_notifikasi_userkey" value="<?php echo esc_attr( get_option('wcfu_sms_notifikasi_userkey', '') ); ?>" />
					</td>
			</tr>
			<tr>
					<th scope="row">Pass Key</th>
					<td>
							<input type="text" id="wcfu_sms_notifikasi_passkey" name="wcfu_sms_notifikasi_passkey" value="<?php echo esc_attr( get_option('wcfu_sms_notifikasi_passkey', '') ); ?>"/>
					</td>
			</tr>
	</table>
</div>

<?php submit_button(); ?>

<script>
	jQuery(document).ready(function(){
		jQuery("#wcfu_sms_notifikasi_enabled").click( function(){
			 if( jQuery(this).is(':checked') ) {
			 		jQuery("#wcfu-notifikasi-sms").show()
			 } else {
				 	jQuery("#wcfu-notifikasi-sms").hide()
			 }
		})
	})
</script>