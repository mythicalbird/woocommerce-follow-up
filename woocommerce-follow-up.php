<?php
/*
Plugin Name: Woocommerce Follow Up
Plugin URI:  https://www.mwt.co.id
Description: Woocommerce Notifikasi & Follow Up
Version:     1.0.0
Author:      Arhi
Author URI:  https://www.mwt.co.id
*/

if ( ! defined( 'WPINC' ) ) {
    die;
}

include_once(ABSPATH . 'wp-admin/includes/plugin.php');
if(!is_plugin_active('woocommerce/woocommerce.php') ) {
  return false;
}

define('WCFU_PATH', __DIR__);
require_once dirname(__FILE__) . '/inc/wcfu-sms-notifikasi.php';
require_once dirname(__FILE__) . '/inc/wcfu-waboxapp.php';
if ( is_admin() ) {
    require_once plugin_dir_path( __FILE__ ) . 'inc/admin.php';
}

function wcfu_notify_order( $order_id ) {

		if ( ! $order_id ) return;
	
		if( get_option( 'wcfu_waboxapp_enabled' ) == 1 ) {
			
				// Getting an instance of the order object
				$order = wc_get_order( $order_id );
				$order_data = $order->get_data();

				$paid = ( $order->is_paid() ) ? 'yes' : 'no';
				
				$whatsapp = new Wcfu_Waboxapp();
				$message = $whatsapp->msg;

				$tag_data = array(
					'customer_name'		=> $order_data['billing']['first_name'] . ' ' . $order_data['billing']['last_name'],
					'customer_phone'	=> $order_data['billing']['phone'],
					'customer_email'	=> $order_data['billing']['email'],
					'invoice_number'	=> '#' . $order->get_id(),
					'order_total'			=> $order_data['currency'] . ' ' . $order_data['total'],
					'order_status'		=> $order_data['status'],
					'order_date'			=> $order_data['date_created']->date('Y-m-d H:i:s'),
					'payment_method'	=> $order_data['payment_method_title'],
					'order_items'	  	=> $order->get_items(),
				);

				$tags = $whatsapp->get_order_message_tags( $tag_data );
			
				$tags_keys = array_keys($tags);
				$tags_values = array_values($tags);
				for( $i = 0; $i < count($tags_keys); $i++ ) {
					$message = str_replace('%'.$tags_keys[$i].'%', $tags_values[$i], $message);
				}

				$nohp = $order_data['billing']['phone'];
				if ( substr($nohp, 0, 2) != "0" ) {
					$nohp = 62 . intval($nohp);
				}
				$unique_id = 'msg-' . time() . $order->get_user_id() . $order->get_id();
				$result = $whatsapp->send_chat( $nohp, $unique_id, $message );
				update_post_meta( $order->get_id(), 'order_notifikasi', 'Terkirim' );
				if( $result['success'] !== true ) {
					update_post_meta( $order->get_id(), 'order_notifikasi', 'Tidak terkirim' );
					// kirim sms
					$sms = new Wcfu_Sms_Notifikasi();
					$sms->send_sms( $nohp, $message );
				}
			
		}
}
add_action('woocommerce_thankyou', 'wcfu_notify_order', 10, 1);


function wcfu_add_order_notifikasi_column_header( $columns ) {

    $new_columns = array();

    foreach ( $columns as $column_name => $column_info ) {

        $new_columns[ $column_name ] = $column_info;

        if ( 'order_total' === $column_name ) {
            $new_columns['order_notifikasi'] = __( 'Notifikasi', 'woocommerce' );
        }
    }

    return $new_columns;
}
add_filter( 'manage_edit-shop_order_columns', 'wcfu_add_order_notifikasi_column_header', 20 );


function wcfu_add_order_notifikasi_column_content( $column ) {
    global $post;

    if ( 'order_notifikasi' === $column ) {
				
				$notifikasi = get_post_meta($post->ID, 'order_notifikasi', true);
				$notifikasi = ( !empty($notifikasi) ) ? $notifikasi : 'Tidak terkirim';
				echo '<span class="notifikasi-order-'.$post->ID.'">';
				if( $notifikasi == 'Terkirim' ) {
					echo '<span class="dashicons dashicons-yes" title="' . $notifikasi . '"></span>';
				} else {
					echo '<a href="#" class="ajax-notifikasi-status" data-id="'.$post->ID.'" onclick="return confirm(\'Notifikasi sudah dikirim?\')">Set terkirim</a>';
				}
				echo '</span>';
    }
}
add_action( 'manage_shop_order_posts_custom_column', 'wcfu_add_order_notifikasi_column_content' );


function wcfu_notifikasi_status_script() { ?>
	<script type="text/javascript" >
	jQuery(document).ready(function($) {
		
		jQuery(".ajax-notifikasi-status").on( 'click', function() {
		
			var data = {
				'action': 'wcfu_notifikasi_status_action',
				'order_id': jQuery(".ajax-notifikasi-status").data('id')
			};

			// since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
			jQuery.post(ajaxurl, data, function(response) {
				jQuery('.notifikasi-order-'+response).html('<span class="dashicons dashicons-yes" title="Terikirim"></span>');
			});
			
		});
		
	});
	</script> <?php
}
add_action( 'admin_footer', 'wcfu_notifikasi_status_script' ); 


function wcfu_notifikasi_status_action() {
		
	if( !empty( $_POST['order_id'] ) ) {
		$order_id = intval($_POST['order_id']);
		$order = wc_get_order( $order_id );

		update_post_meta( $order->get_id(), 'order_notifikasi', 'Terkirim' );

		echo $order_id;
	}

	wp_die(); // this is required to terminate immediately and return a proper response
}
add_action( 'wp_ajax_wcfu_notifikasi_status_action', 'wcfu_notifikasi_status_action' );


function wcfu_add_cron_interval( $schedules ) {
    $schedules['five_seconds'] = array(
        'interval' => 30,
        'display'  => esc_html__( 'Every Five Minutes' ),
    );
 
    return $schedules;
}
add_filter( 'cron_schedules', 'wcfu_add_cron_interval' );


function wcfu_follow_up_cronjob() {
	
	$delay = intval( get_option('wcfu_notifikasi_delay', 1) );
	
	if( get_option( 'wcfu_notifikasi_enabled' ) == 1 ) {

		$current_time = current_time('H:i');
		$time = sprintf( "%02d", trim( get_option('wcfu_notifikasi_jam', '00') ) );
		$time .= ':';
		$time .= trim( get_option('wcfu_notifikasi_menit', '00') );
		
		if( $current_time == $time ) {

			$orders = wc_get_orders( array(
					'status' => get_option('wcfu_notifikasi_order_status'),
			) );

			$whatsapp = new Wcfu_Waboxapp();
			$sms = new Wcfu_Sms_Notifikasi();

			foreach( $orders as $order ) {

				$message = get_option('wcfu_notifikasi_pesan');

				$order_data = $order->get_data();

				$tag_data = array(
					'customer_name'		=> $order_data['billing']['first_name'] . ' ' . $order_data['billing']['last_name'],
					'customer_phone'	=> $order_data['billing']['phone'],
					'customer_email'	=> $order_data['billing']['email'],
					'invoice_number'	=> '#' . $order->get_id(),
					'order_total'			=> $order_data['currency'] . ' ' . $order_data['total'],
					'order_status'		=> $order_data['status'],
					'order_date'			=> $order_data['date_created']->date('Y-m-d H:i:s'),
					'payment_method'	=> $order_data['payment_method_title'],
					'order_items'		=> $order->get_items(),
				);

				$tags = $whatsapp->get_order_message_tags( $tag_data );
				$tags_keys = array_keys($tags);
				$tags_values = array_values($tags);
				for( $i = 0; $i < count($tags_keys); $i++ ) {
					$message = str_replace('%'.$tags_keys[$i].'%', $tags_values[$i], $message);
				}

				$nohp = $order_data['billing']['phone'];
				if ( substr($nohp, 0, 2) != "0" ) {
					$nohp = 62 . intval($nohp);
				}

				$unique_id = 'fup-' . time() . $order->get_user_id() . $order->get_id();

				$result = $whatsapp->send_chat( $nohp, $unique_id, $message );
				update_post_meta( $order->get_id(), 'order_notifikasi', 'Terkirim' );

				if( $result['success'] !== true ) {

					update_post_meta( $order->get_id(), 'order_notifikasi', 'Tidak terkirim' );
					// kirim sms
					$sms->send_sms( $nohp, $message );

				}

				sleep( $delay );

			}
			
		}
		
	}
	
}
add_action( 'wcfu_follow_up_hook', 'wcfu_follow_up_cronjob' );